exports.LocalDate = function() {
    var d = new Date();
    var offset = (d.getTimezoneOffset() / 60) * -1;
    console.log("offset: %n", offset);
    var n = new Date(d.getTime() + offset*1000*60*60);
    return n;
};