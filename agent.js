var Tinkerforge = require('tinkerforge');
var API = require('./api');

var Config = require('./config');

var HOST = Config['tinkerforge']['host'];
var PORT = Config['tinkerforge']['port'];

var ipcon = new Tinkerforge.IPConnection();

var bTemperature = new Tinkerforge.BrickletTemperature(Config['tinkerforge']['uid']['temperature'], ipcon);
var bHumidity = new Tinkerforge.BrickletHumidity(Config['tinkerforge']['uid']['humidity'], ipcon);
var bMoisture = new Tinkerforge.BrickletMoisture (Config['tinkerforge']['uid']['moisture'], ipcon);
var bAmbientLight = new Tinkerforge.BrickletAmbientLight(Config['tinkerforge']['uid']['ambientLight'], ipcon);

ipcon.connect(HOST, PORT,
    function(error) {
        console.log('Error: '+error);
    }
); // Connect to brickd
// Don't use device before ipcon is connected

ipcon.on(Tinkerforge.IPConnection.CALLBACK_CONNECTED,
    function(connectReason) {

        var refreshPeriod = Config['tinkerforge']['refreshPeriod'];

        bTemperature.setTemperatureCallbackPeriod(refreshPeriod);
        bHumidity.setHumidityCallbackPeriod(refreshPeriod);
        bMoisture.setMoistureCallbackPeriod(refreshPeriod);
        bAmbientLight.setIlluminanceCallbackPeriod(refreshPeriod);

    }
);

// Register temperature callback
bTemperature.on(Tinkerforge.BrickletTemperature.CALLBACK_TEMPERATURE,
    // Callback function for temperature callback (parameter has unit °C/100)
    function(temp) {
        console.log('Temperature: '+temp/100+' °C');

        API.writeSensorDataToAPI("temperature", temp, "1/100", Config['tinkerforge']['uid']['temperature'], function(responsebody) {
            console.log("Body: %j", responsebody);

        });
    }
);

// Register humidity callback
bHumidity.on(Tinkerforge.BrickletHumidity.CALLBACK_HUMIDITY,
    // Callback function for humidity callback (parameter has unit %RH/10)
    function(humidity) {
        console.log('Relative Humidity: '+humidity/10+' %RH');

        API.writeSensorDataToAPI("humidity", humidity, "1/10", Config['tinkerforge']['uid']['humidity'], function(responsebody) {
            console.log("Body: %j", responsebody);

        });
    }
);

// Register moisture callback
bMoisture.on(Tinkerforge.BrickletMoisture.CALLBACK_MOISTURE,
    // Callback function for moisture value
    function(moisture) {
        console.log('Moisture Value: '+moisture);

        API.writeSensorDataToAPI("moisture", moisture, "1", Config['tinkerforge']['uid']['moisture'], function(responsebody) {
            console.log("Body: %j", responsebody);

        });
    }
);

// Register ambient light callback
bAmbientLight.on(Tinkerforge.BrickletAmbientLight.CALLBACK_ILLUMINANCE,
    // Callback function for illuminance callback (parameter has unit Lux/10)
    function(illuminance) {
        console.log('Illuminance: '+illuminance/10+' Lux');

        API.writeSensorDataToAPI("ambientLight", illuminance, "1/10", Config['tinkerforge']['uid']['ambientLight'], function(responsebody) {
            console.log("Body: %j", responsebody);

        });
    }
);

// End stuff
console.log("^C to exit ...");
process.stdin.on('data',
    function(data) {
        ipcon.disconnect();
        process.exit(0);
    }
);
