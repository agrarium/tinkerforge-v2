# README #

Software running locally on Tinkerforge device to push sensor data to our API via Wifi.

### How to run while programming ###

Connect the [RED Brick](http://www.tinkerforge.com/de/doc/Tutorials/Tutorial_RED_Brick/Tutorial.html#tutorial-red-brick) of the Tinkerforge device via USB to your machine, after having install the [Brick Deamon (brickd)](http://www.tinkerforge.com/de/doc/Software/Brickd.html).

Check out repo, and change the `config.json` to match your device and sensor IDs.

Go to the folder via your Terminal and run:


```
#!bash

npm install
node agent.js
```


### How to install on Tinkerforge ###

You can use the [Brick Viewer (brickv)](http://www.tinkerforge.com/de/doc/Software/Brickv.html#brickv-installation) to access the RED Bricks management console. There you can [install programs](http://www.tinkerforge.com/de/doc/Hardware/Bricks/RED_Brick_Program_Tab.html#red-brick-program-tab).
