var Request = require('request');

var Helper = require('./helper');

var Config = require('./config');

exports.writeSensorDataToAPI = function(type, value, resolution, sensor, callback) {

    var date = Helper.LocalDate();

    // prepare request
    var options = {
        url: Config['api']['url'] + '/sensorEvent',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        json: true,
        body: {
            type: type,
            value: value,
            resolution: resolution,
            sensor: sensor,
            timestamp: date
        }
    };

    Request(options, function (error, response, body) {
      console.log(error);

    });

}
